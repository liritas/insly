var inputsValidation = {};
document.addEventListener("DOMContentLoaded", bootstrap);


function bootstrap() {
    const inputs = getInputs();
    inputs.forEach(elem => elem.addEventListener("change", validateInput));
}

function validateInput(input) {
    const elem = input.target;
    const name = elem.name;
    const value = elem.value;
    switch (name) {
        case 'estimated_value':
            if (value > 99 && value < 100001) {
                onValid(elem);
            } else {
                showError(elem);
            }
            break;
        case 'tax_percentage':
            if (value >= 0 && value <= 100) {
                onValid(elem);
            } else {
                showError(elem);
            }
            break;
        case 'number_of_instalments':
            if (value > 0 && value < 13) {
                onValid(elem);
            } else {
                showError(elem);
            }
            break;
    }
}

function validateForm() {
    if (
        inputsValidation.estimated_value &&
        inputsValidation.tax_percentage &&
        inputsValidation.number_of_instalments
    ) {
        return true;
    } else {
        return false;
    }
}

function getInputs() {
    const inputs = document.querySelectorAll('input[type=text]');

    return inputs;
}


function showError(element) {
    element.className = 'wrong';
    inputsValidation[element.name] = false;
}


function onValid(element) {
    element.className = '';
    inputsValidation[element.name] = true;
}