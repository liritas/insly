<?php

require_once 'src/policy.entity.php';

use Policy as Policy; 

class Calculator {
    private $params;

    function __construct() {
        $this->params = $this->getParameters();
    }

    // getting parameters
    private function getParameters(): array {
        if (!isset($_GET['estimated_value']) || !isset($_GET['tax_percentage']) || !isset($_GET['number_of_instalments'])) {
            throw new ErrorException('Need more parameters.');
        }
        $estimatedValue = (int)$_GET['estimated_value'];
        $taxPercentage = (int)$_GET['tax_percentage'];
        $numberOfInstalments = (int)$_GET['number_of_instalments'];

        return [
            'estimated_value' => $estimatedValue,
            'tax_percentage' => $taxPercentage,
            'number_of_instalments' => $numberOfInstalments,
        ];
    }

    public function calculate() {
        $policy = new Policy;
        $policy->setValue = $this->params['estimated_value'];
        $policy->setComission = $this->params['estimated_value'];
        $policy->setTax = $this->params['estimated_value'];
    }

    public function printResult() {

    }
}


$calculator = new Calculator;
$calculator->calculate();
