<?php

require_once 'policy.interface.php';

use PolicyInterface as PolicyInterface;

class Policy implements PolicyInterface
{
    private $value;
    private $basePrice;
    private $comission;
    private $tax;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue(int $value)
    {
        // incapsulate base value inside scope
        if ($value >= 100 && $value <= 100000) {
            $this->value = $value;
        }
    }

    public function getBasePrice()
    {
        return $this->basePrice;
    }

    public function setBasePrice(int $basePrice)
    {
        $this->basePrice = $basePrice;
    }

    public function getComission()
    {
        return $this->comission;
    }

    public function setComission(int $comission)
    {
        $this->comission = $comission;
    }

    public function getTax()
    {
        return $this->tax;
    }

    public function setTax(int $tax)
    {
        $this->tax = $tax;
    }
}
