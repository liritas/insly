<?php

interface PolicyInterface
{
    public function getValue();
    public function setValue(int $value);
    public function getBasePrice();
    public function setBasePrice(int $basePrice);
    public function getComission();
    public function setComission(int $comission);
    public function getTax();
    public function setTax(int $tax);

}
