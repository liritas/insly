<?php

const TASK_BIN = '01110000 01110010 01101001 01101110 01110100 00100000 01101111 01110101 01110100 00100000 01111001 01101111 01110101 01110010 00100000 01101110 01100001 01101101 01100101 00100000 01110111 01101001 01110100 01101000 00100000 01101111 01101110 01100101 00100000 01101111 01100110 00100000 01110000 01101000 01110000 00100000 01101100 01101111 01101111 01110000 01110011';
const MY_NAME = 'Dzmitry Sharko';

echo getStringFromBin(TASK_BIN).'<br />'; // print out your name with one of php loops
echo getBinFromString(MY_NAME); // 1000100 1111010 1101101 1101001 1110100 1110010 1111001 100000 1010011 1101000 1100001 1110010 1101011 1101111


function getStringFromBin($bin)
{
    $str = '';
    $binArray = explode(' ', $bin);
    foreach ($binArray as $binItem) {
        $str .= chr(bindec($binItem));
    }

    return $str;
}


function getBinFromString($str) {
    $strSymbols = str_split($str);
    $binName = [];
    foreach ($strSymbols as $symbol) {
        $binName[] = decbin(ord($symbol));
    }

    return join(' ', $binName);
}
